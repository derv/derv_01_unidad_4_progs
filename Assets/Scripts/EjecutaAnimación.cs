using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjecutaAnimación : MonoBehaviour
{
    [SerializeField]
    GameObject personaje;

    Animator animator;

    private void Start()
    {
        animator = personaje.GetComponent<Animator>();
    }

    public void playAnimacion1()
    {
        animator.Play("Idle");
    }

    public void playAnimacion2()
    {
        animator.Play("SpellCast #1");
    }

    public void playAnimacion3()
    {
        animator.Play("SpellCast #2");
    }
}
